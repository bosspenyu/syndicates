<x-laravel-ui-adminlte::adminlte-layout>
    <body class="hold-transition login-page">
    <img src="{{asset('mpob_logo.png')}}" width="200" height="200" style="border-radius: 35%;" class="img-fluid" alt="">
        <div class="login-box mt-4">
            <!-- /.login-box-body -->
            <div class="card card-outline card-olive">
                <div class="card-header text-center">
                    <a href="#" class="h3">Sindiket</a>
                </div>
                <div class="card-body login-card-body">
                    <p class="login-box-msg">
                        @include('elements.alert')
                    </p>

                    <form method="post" action="{{ url('/login') }}">
                        @csrf

                        <div class="input-group mb-3">
                            <input type="email" name="email" value="{{ old('email') }}" placeholder="Email"
                                class="form-control @error('email') is-invalid @enderror">
                            <div class="input-group-append">
                                <div class="input-group-text"><span class="fas fa-envelope"></span></div>
                            </div>
                            @error('email')
                                <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="input-group mb-3">
                            <input type="password" name="password" placeholder="{{ __('Katalaluan') }}"
                                class="form-control @error('password') is-invalid @enderror">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                            @error('password')
                                <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror

                        </div>

                        <div class="row">
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-xs btn-block">{{ __('Log Masuk') }}</button>
                            </div>

                        </div>
                    </form>
                </div>
                <!-- /.login-card-body -->
            </div>

        </div>
        <!-- /.login-box -->
    </body>
</x-laravel-ui-adminlte::adminlte-layout>
