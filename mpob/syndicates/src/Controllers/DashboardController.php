<?php

namespace Mpob\Syndicates\Controller;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
	public function index()
	{
		return view('dashboard');
	}
}
