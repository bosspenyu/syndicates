<?php

namespace Mpob\Syndicates\App\Models;

use Illuminate\Database\Eloquent\Model;

class SyndicateType extends Model
{
    public const CREATED_AT = 'create_dt';
    public const UPDATED_AT = 'update_dt';
}
