<?php

namespace Mpob\Syndicates\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrcTxrptTrade extends Model
{
    use HasFactory;

    protected $table = "trc_txrpt_trade";
}
