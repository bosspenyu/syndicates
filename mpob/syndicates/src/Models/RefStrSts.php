<?php

namespace Mpob\Syndicates\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Status Untuk Pengesahan
 */
class RefStrSts extends Model
{
    use HasFactory;
}
