<?php

namespace Mpob\Syndicates\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefStsCmn extends Model
{
    use HasFactory;

    protected $table = "ref_sts_cmn";
}
