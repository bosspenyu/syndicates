<?php

namespace Mpob\Syndicates\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExtLcn extends Model
{
    use HasFactory;

    protected $primaryKey = "id_";
}
