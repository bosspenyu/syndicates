<?php

namespace Mpob\Syndicates\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrcAccRole extends Model
{
    use HasFactory;

    protected $table = "trc_acc_role";
}
