<?php

namespace Mpob\Syndicates\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrcAccVehicle extends Model
{
    use HasFactory;
    protected $primaryKey = "id_";
    protected $table ="trc_acc_vehicle";
}
