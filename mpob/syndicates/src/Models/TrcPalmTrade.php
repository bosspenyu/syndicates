<?php

namespace Mpob\Syndicates\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrcPalmTrade extends Model
{
    use HasFactory;

    protected $table = "trc_palm_trade";
}
